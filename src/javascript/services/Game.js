export class Game {
    constructor() {
        this.isOver = false;
        this.winner = null;
    }
    updateGame(winner) {
        this.isOver = true;
        this.winner = winner;
    }
}