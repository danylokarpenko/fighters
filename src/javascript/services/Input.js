export class Input {
    constructor(controls, playerOne, playerTwo) {
        document.addEventListener('keydown', (event) => {
            const { code } = event;
            switch (code) {
                case controls.PlayerOneAttack:
                    playerOne.hit(playerTwo);
                    break;
                case controls.PlayerTwoAttack:
                    playerTwo.hit(playerOne);
                    break;
                case controls.PlayerOneBlock:
                    playerOne.blockOn = true;
                    break;
                case controls.PlayerTwoBlock:
                    playerTwo.blockOn = true;
                    break;
            }
        })
        document.addEventListener('keyup', (event) => {
            const { code } = event;
            switch (code) {
                case controls.PlayerOneBlock:
                    playerOne.blockOn = false;
                    break;
                case controls.PlayerTwoBlock:
                    playerTwo.blockOn = false;
                    break;
            }
        })
        this.runOnKeyCombination(() => {
            playerOne.criticalHit(playerTwo);
        }, controls.PlayerOneCriticalHitCombination);

        this.runOnKeyCombination(() => {
            playerTwo.criticalHit(playerOne);
        }, controls.PlayerTwoCriticalHitCombination);
    }

    includesCombination(pressedKeys, combination) {
        const isComboPressed = combination.every(key => pressedKeys.has(key));
        console.log(isComboPressed, pressedKeys);
        return isComboPressed;
    }

    runOnKeyCombination(func, codes) {
        const pressed = new Set();
        document.addEventListener('keydown', function (event) {
            pressed.add(event.code);
            for (let code of codes) {
                if (!pressed.has(code)) {
                    return;
                }
            }
            pressed.clear();
            func();
        });
        document.addEventListener('keyup', function (event) {
            pressed.delete(event.code);
        });
    }
} 