import { getDamage, getCriticalDamage } from "../components/fight";

export class Player {
    constructor(fighter, healthIndicator, game) {
        this.fighter = fighter;
        this.healthIndicator = healthIndicator;
        this.game = game;
        this.blockOn = false;
        this.health = fighter.health
        this.criticalAvaible = true;
    }
    hit(opponent) {
        if (this.blockOn) return;
        const damage = getDamage(this.fighter, opponent.fighter);
        if (!opponent.blockOn) {
            opponent.health -= damage;
            opponent.setIndicatorWidth(opponent.health);
        }
        if (opponent.health <= 0) {
            this.game.updateGame(this.fighter);
        }
    }
    criticalHit(opponent) {        
        if (this.blockOn || !this.criticalAvaible) return;
        const damage = getCriticalDamage(this.fighter);
        opponent.health -= damage;
        opponent.setIndicatorWidth(opponent.health);
        if (opponent.health <= 0) {
            this.game.updateGame(this.fighter);
        }
        this.criticalAvaible = false;
        setTimeout(() => this.criticalAvaible = true, 10000);
    }
    setIndicatorWidth(health) {
        const healthPercentage = 100 * health / this.fighter.health;
        this.healthIndicator.style.width = `${healthPercentage}%`;
    }
}