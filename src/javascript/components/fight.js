import { controls } from '../../constants/controls';
import { Player } from '../services/Player';
import { Input } from '../services/Input';
import { Game } from '../services/Game';

function getIndicatorElementById(id) {
  return document.getElementById(id);
}

export async function fight(firstFighter, secondFighter) {
  const leftHealthIndicator = getIndicatorElementById('left-fighter-indicator');
  const rightHealthIndicator = getIndicatorElementById('right-fighter-indicator');

  const game = new Game();

  const playerOne = new Player(firstFighter, leftHealthIndicator, game);
  const playerTwo = new Player(secondFighter, rightHealthIndicator, game);

  new Input(controls, playerOne, playerTwo);

  while (!game.isOver) {
    await gameLoop();
  }

  return new Promise((resolve) => {
    resolve(game.winner);
  });
}

export function getCriticalDamage(attacker) {
  const damage = 2 * attacker.attack;
  return damage;
}
export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}
export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = Math.random() * (2 - 1) + 1;
  const power = attack * criticalHitChance;
  return power;
}
export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = Math.random() * (2 - 1) + 1;
  const power = defense * dodgeChance;
  return power;
}

function gameLoop(ms = 100) {
  return new Promise(
    resolve => setTimeout(resolve, ms)
  );
}