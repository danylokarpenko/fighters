import { createElement } from '../../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const { source, name } = fighter;
  const title = `${name} is the winner!`;

  const attributes = {
    src: source,
    title,
    alt: `winner-${name}`,
  };
  const bodyElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  showModal({ title, bodyElement });
}
